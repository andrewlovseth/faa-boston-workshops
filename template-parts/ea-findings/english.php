<div class="tab active" id="en">

    <section class="ea-findings-table table grid">
        
            <div class="headline section-header">
                <h1><?php echo get_field('page_header'); ?></h1>
                <h3><?php echo get_field('table_headline'); ?></h3>
            </div>

            <div class="header-row row p2">
                <?php $columns = array(); if(have_rows('table_header_columns')): while(have_rows('table_header_columns')): the_row(); ?>
                    <?php 
                        $data = get_sub_field('column');
                        $label = $data['label'];
                        $content = $data['content'];
                        array_push($columns, $content);                
                    ?>
                
                    <div class="<?php echo $label; ?>">
                        <?php echo $content; ?>
                    </div>
                <?php endwhile; endif; ?>			
            </div>

            <?php if(have_rows('table_rows')): while(have_rows('table_rows')): the_row(); ?>
                <div class="row data-row p3">
                    <?php $count = 0; if(have_rows('rows')): while(have_rows('rows')): the_row(); ?>
                    
                        <?php 
                            $data = get_sub_field('column');
                            $label = $data['label'];
                            $content = $data['content'];
                        ?>
                    
                        <div class="<?php echo $label; ?>" data-label="<?php echo $columns[$count]; ?>">
                            <?php echo $content; ?>
                        </div>
                        
                    <?php $count++; endwhile; endif; ?>
                </div>
            <?php endwhile; endif; ?>	
            
    </section>

</div>
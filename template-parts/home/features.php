<section class="features grid">
    <div class="features-flex">
  
        <?php if(have_rows('features')): while(have_rows('features')) : the_row(); ?>

            <?php if( get_row_layout() == 'feature' ): ?>

                <div class="feature">
                    <div class="icon">
                        <img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>

                    <div class="headline">
                        <h3><?php echo get_sub_field('headline'); ?></h3>
                    </div>

                    <div class="copy p3">
                        <?php echo get_sub_field('copy'); ?>
                    </div>

                    <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <div class="cta">
                            <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        </div>

                    <?php endif; ?>
                    
                </div>

            <?php endif; ?>

        <?php endwhile; endif; ?>

    </div>
</section>
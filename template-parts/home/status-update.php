<?php

$status_update = get_field('status_update');
$show = $status_update['show'];

if($show == TRUE):
    
if(have_rows('status_update')): while(have_rows('status_update')): the_row(); ?>

    <section class="status-update grid">
        <div class="update-wrapper">
            <?php if(have_rows('updates')): while(have_rows('updates')): the_row(); ?>
                
                <div class="entry">
                    <div class="headline">
                        <h4><?php echo get_sub_field('headline'); ?></h4>
                    </div>

                    <div class="copy p2">
                        <?php echo get_sub_field('copy'); ?>
                    </div>
                </div>

            <?php endwhile; endif; ?>
        </div>
    </section>

    <?php endwhile; endif; ?>

<?php endif; ?>


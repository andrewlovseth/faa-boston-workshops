<section class="full-report grid">

    <div class="download-report">

        <?php
            $final_report = get_field('final_report', 'options');
            $final_file = $final_report['file'];
            $final_cta = $final_report['cta'];
            $final_details = $final_report['details'];
            
            if( $final_file ):
        ?>

            <a href="<?php echo $final_file['url']; ?>" rel="external">
                <span class="icon">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-pdf-large-white.svg" alt="PDF" />
                </span>

                <span class="meta">
                    <span class="cta"><?php echo $final_cta; ?></span>
                    <?php if($final_details): ?>
                        <span class="details"><?php echo $final_details; ?></span>
                    <?php endif; ?>
                </span>
            </a>

        <?php endif; ?>


        <?php
            $draft_file = get_field('full_report', 'options');
            $draft_cta = get_field('full_report_cta', 'options');
            $draft_details = get_field('full_report_details', 'options');
            
            if( $draft_file ):
        ?>

            <a href="<?php echo $draft_file['url']; ?>" rel="external">
                <span class="icon">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-pdf-large-white.svg" alt="PDF" />
                </span>

                <span class="meta">
                    <span class="cta"><?php echo $draft_cta; ?></span>
                    <?php if($draft_details): ?>
                        <span class="details"><?php echo $draft_details; ?></span>
                    <?php endif; ?>
                </span>
            </a>

        <?php endif; ?>

    </div>

</section>
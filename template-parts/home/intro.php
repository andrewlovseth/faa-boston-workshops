<section class="intro grid">
    <div class="headline">
            <h3><?php echo get_field('intro_headline'); ?></h3>
    </div>

    <div class="info">
        <div class="copy p2">
            <?php echo get_field('intro_copy'); ?>
        </div>

        <?php 
            $link = get_field('intro_link');
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <div class="cta">
                <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            </div>

        <?php endif; ?>
    </div>
    
    <div class="video">
        <a href="#" class="video-trigger" data-video-id="<?php echo get_field('intro_video_id'); ?>" data-video-type="vimeo">
            <?php $image = get_field('intro_video_thumbnail'); if( $image ): ?>
                <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
            <?php endif; ?>
        </a>
    </div>    
</section>
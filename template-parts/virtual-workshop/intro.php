<section class="intro grid">
    <div class="headline">
        <h2><?php echo get_field('intro_headline'); ?></h2>
    </div>

    <div class="copy p2">
        <?php echo get_field('intro_copy'); ?>
    </div>
</section>
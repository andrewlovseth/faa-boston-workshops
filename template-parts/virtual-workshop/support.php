<section class="support grid">
    <div class="headline">
        <h2><?php echo get_field('support_headline'); ?></h2>
    </div>

    <div class="copy p2">
        <?php echo get_field('support_copy'); ?>
    </div>
</section>
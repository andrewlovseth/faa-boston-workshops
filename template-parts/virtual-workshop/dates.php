<div class="dates grid">
    <div class="info">
        <div class="headline section-header">
            <h4><?php echo get_field('dates_headline'); ?></h4>
        </div>

        <div class="options">
            <?php if(have_rows('dates')): while(have_rows('dates')): the_row(); ?>            
                <div class="option headline">
                    <h3><?php echo get_sub_field('date'); ?></h3>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</div>
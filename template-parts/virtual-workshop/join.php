<section class="join grid">
    <div class="headline">
        <h2><?php echo get_field('join_headline'); ?></h2>
    </div>

    <div class="copy p2">
        <?php echo get_field('join_copy'); ?>
    </div>
</section>
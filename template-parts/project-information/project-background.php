<section class="project-background grid" id="<?php echo sanitize_title_with_dashes(get_field('project_background_headline')); ?>">
    <div class="headline black">
        <h2><?php echo get_field('project_background_headline'); ?></h2>
    </div>

    <div class="video">
        <div class="content">
            <a href="#" class="video-trigger" data-video-id="<?php echo get_field('project_background_video_id'); ?>" data-video-type="vimeo">
                <img src="<?php $image = get_field('project_background_video_thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </a>
        </div>
    </div>

    <?php get_template_part('template-parts/global/language-switcher'); ?>

    <div class="tab active" id="en">
        <div class="copy p2">
            <?php echo get_field('project_background_copy'); ?>
        </div>    
    </div>

    <div class="tab" id="es">
        <div class="copy p2">
            <?php echo get_field('project_background_spanish'); ?>
        </div>    
    </div>

</section>
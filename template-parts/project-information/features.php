<?php if(have_rows('features')): while(have_rows('features')) : the_row(); ?>
    <?php if( get_row_layout() == 'feature' ): ?>

        <section class="feature grid" id="<?php echo sanitize_title_with_dashes(get_sub_field('headline')); ?>">
            <div class="sub-grid">
                <div class="photo">
                    <div class="content">
                        <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>
                </div>

                <div class="info">
                    <div class="headline black">
                        <h2><?php echo get_sub_field('headline'); ?></h2>
                    </div>

                    <div class="copy p2">
                        <?php echo get_sub_field('copy'); ?>
                    </div>

                    <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <div class="cta">
                            <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        </div>           

                    <?php endif; ?>
                </div>    

            </div>        
        </section>

    <?php endif; ?>
<?php endwhile; endif; ?>
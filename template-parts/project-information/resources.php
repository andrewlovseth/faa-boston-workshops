<section class="resources grid" id="<?php echo sanitize_title_with_dashes(get_field('resources_headline')); ?>">
    <div class="headline black">
        <h2><?php echo get_field('resources_headline'); ?></h2>
    </div>

    <div class="copy p1">
        <?php echo get_field('resources_copy'); ?>
    </div>

    <div class="links p1">
        <?php if(have_rows('resources')): while(have_rows('resources')): the_row(); ?>
        
            <?php 
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="resource">
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                </div>
            
            <?php endif; ?>

        <?php endwhile; endif; ?>
    </div>
</section>
<section class="intro grid">
    <div class="headline">
        <h1><?php echo get_field('page_header'); ?></h1>
        <h2><?php echo get_field('intro_headline'); ?></h2>
    </div>

    <div class="copy p2 extended">
        <?php echo get_field('intro_copy'); ?>
    </div>
</section>
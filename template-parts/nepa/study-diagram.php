<section class="study-diagram diagram-section grid">
    <div class="info">
        <div class="headline">
            <h2><?php echo get_field('study_diagram_headline'); ?></h2>
        </div>

        <div class="copy p2 extended">
            <?php echo get_field('study_diagram_copy'); ?>
        </div>
    </div>

    <div class="graphic">
        <img src="<?php $image = get_field('study_diagram_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    </div>
</section>
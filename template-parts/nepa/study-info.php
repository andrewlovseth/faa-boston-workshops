<section class="study-info info-section grid">
    <div class="headline">
        <h2><?php echo get_field('study_info_headline'); ?></h2>
    </div>

    <div class="copy p2 extended">
        <?php echo get_field('study_info_copy'); ?>
    </div>
</section>
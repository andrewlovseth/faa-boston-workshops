<section class="sound-level grid">
    <div class="info">
        <div class="headline">
            <h2><?php echo get_field('sound_level_headline'); ?></h2>
        </div>

        <div class="copy p2">
            <?php echo get_field('sound_level_copy'); ?>
        </div>
    </div>

    <div class="graphic">
        <?php if(have_rows('sound_level_graphics')): while(have_rows('sound_level_graphics')): the_row(); ?>
            <img src="<?php $image = get_sub_field('graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endwhile; endif; ?>
    </div>
</section>
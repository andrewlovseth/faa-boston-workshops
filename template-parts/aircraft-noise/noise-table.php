<section class="noise-table table grid">
    <div class="header-row row p2">
        <?php $columns = array(); if(have_rows('noise_table_header_columns')): while(have_rows('noise_table_header_columns')): the_row(); ?>
            <?php 
                $data = get_sub_field('column');
                $label = $data['label'];
                $content = $data['content'];
                array_push($columns, $content);
            ?>
        
            <div class="<?php echo $label; ?>">
                <?php echo $content; ?>
            </div>
        <?php endwhile; endif; ?>			
    </div>

    <?php if(have_rows('noise_table_rows')): while(have_rows('noise_table_rows')): the_row(); ?>
        <div class="row data-row p3">
            <?php $count = 0; if(have_rows('rows')): while(have_rows('rows')): the_row(); ?>
            
                <?php 
                    $data = get_sub_field('column');
                    $label = $data['label'];
                    $content = $data['content'];
                ?>
            
                <div class="<?php echo $label; ?>" data-label="<?php echo esc_attr(wp_strip_all_tags($columns[$count])); ?>">
                    <?php echo $content; ?>
                </div>
                
            <?php $count++; endwhile; endif; ?>
        </div>
    <?php endwhile; endif; ?>			
</section>

<?php 
	$link = get_field('link');
	if( $link ): 
	$link_url = $link['url'];
	$link_title = $link['title'];
	$link_target = $link['target'] ? $link['target'] : '_self';
 ?>

 	<div class="cta">
 		<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
 	</div>

<?php endif; ?>
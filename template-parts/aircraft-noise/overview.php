<section class="overview grid">
    <div class="info">
        <div class="headline">
            <h2><?php echo get_field('overview_headline'); ?></h2>
        </div>

        <div class="copy p2">
            <?php echo get_field('overview_copy'); ?>
        </div>
    </div>

    <div class="graphic">
        <img src="<?php $image = get_field('overview_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    </div>
</section>
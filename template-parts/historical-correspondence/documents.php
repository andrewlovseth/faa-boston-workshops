<?php

    $documents = get_field('documents');
    $headline = $documents['headline'];
    $copy = $documents['copy'];

if(have_rows('documents')): while(have_rows('documents')): the_row(); ?>

    <aside class="documents">
        <div class="headline">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="copy p3 extended">
            <div class="intro">
                <?php echo $copy; ?>
            </div>

            <div class="list">
                <?php if(have_rows('list')): while(have_rows('list')) : the_row(); ?>

                    <?php if( get_row_layout() == 'header' ): ?>
                        <div class="header">
                            <h4><?php echo get_sub_field('text'); ?></h4>
                        </div>
                    <?php endif; ?>

                    <?php if( get_row_layout() == 'sub_header' ): ?>
                        <div class="sub-header">
                            <h5><?php echo get_sub_field('text'); ?></h5>
                        </div>
                    <?php endif; ?>

                    <?php if( get_row_layout() == 'items' ): ?>
                        <?php if(have_rows('documents')): ?>
                            <ul class="items">
                                <?php while(have_rows('documents')): the_row(); ?>
                                    <li>
                                        <a href="<?php echo get_sub_field('file'); ?>" rel="external"><?php echo get_sub_field('title'); ?></a>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                    <?php endif; ?>

                <?php endwhile; endif; ?>
            </div>

        </div>
    </aside>

<?php endwhile; endif; ?>

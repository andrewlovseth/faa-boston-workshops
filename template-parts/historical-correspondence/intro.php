<?php

    $intro = get_field('intro');
    $headline = $intro['headline'];
    $copy = $intro['copy'];

?>

<div class="intro">
	<div class="headline">
		<h2><?php echo $headline; ?></h2>
	</div>

	<div class="copy p2 extended">
		<?php echo $copy; ?>
	</div>
</div>
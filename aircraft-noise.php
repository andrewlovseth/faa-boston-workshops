<?php

/*
	Template Name: Aircraft Noise
*/

get_header(); ?>

	<?php get_template_part('template-parts/aircraft-noise/overview'); ?>

	<?php get_template_part('template-parts/aircraft-noise/noise-table'); ?>

	<?php get_template_part('template-parts/aircraft-noise/sound-level'); ?>

<?php get_footer(); ?>
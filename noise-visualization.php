<?php

/*
	Template Name: Noise Visualization
*/

get_header(); ?>

	<section class="visualization grid">

		<div class="headline">
			<h2><?php echo get_field('headline'); ?></h2>
		</div>

		<div class="copy p2 extended">
			<?php echo get_field('copy'); ?>
		</div>

		<div class="embed">
			<?php echo get_field('embed'); ?>
		</div>

	</section>

<?php get_footer(); ?>
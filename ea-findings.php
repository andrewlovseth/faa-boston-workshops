<?php

/*
	Template Name: EA Findings
*/

get_header(); ?>

    <?php get_template_part('template-parts/ea-findings/intro'); ?>

    <?php get_template_part('template-parts/ea-findings/english'); ?>

    <?php get_template_part('template-parts/ea-findings/spanish'); ?>

<?php get_footer(); ?>
<?php

/*
	Template Name: Comments
*/

get_header(); ?>

<section class="comments grid">

	<div class="info">
		<div class="headline">
			<h2><?php echo get_field('headline'); ?></h2>
		</div>

		<div class="copy p2 extended">
			<?php echo get_field('copy'); ?>
		</div>
	</div>

	<div class="cta">
		<div class="circle">
			<div class="content">
				<?php 
					$icon = get_field('comments_link_icon'); 
					$link = get_field('comments_link');
					if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
					<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
						<span class="icon"><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" /></span>
						<span class="label"><?php echo esc_html($link_title); ?></span>
					</a>

				<?php endif; ?>
			</div>
		</div>
	</div>

</section>

<?php get_footer(); ?>
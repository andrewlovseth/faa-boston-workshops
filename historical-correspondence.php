<?php

/*
	Template Name: Historical Correspondence
*/

get_header(); ?>

    <section class="main grid">

        <?php get_template_part('template-parts/historical-correspondence/intro'); ?>
        
        <?php get_template_part('template-parts/historical-correspondence/documents'); ?>
        
    </section>

<?php get_footer(); ?>
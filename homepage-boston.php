<?php

/*
	Template Name: Home - Boston
*/

get_header(); ?>

    <?php get_template_part('template-parts/home/status-update'); ?>

    <?php get_template_part('template-parts/home/intro'); ?>

    <?php get_template_part('template-parts/home/download-report'); ?>

    <?php get_template_part('template-parts/home/features'); ?>

    <?php get_template_part('template-parts/global/video-overlay'); ?>

<?php get_footer(); ?>
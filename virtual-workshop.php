<?php

/*
	Template Name: Virtual Workshop
*/

get_header(); ?>

	<?php get_template_part('template-parts/virtual-workshop/intro'); ?>

	<?php get_template_part('template-parts/virtual-workshop/dates'); ?>

	<?php get_template_part('template-parts/virtual-workshop/join'); ?>

	<?php get_template_part('template-parts/virtual-workshop/support'); ?>

<?php get_footer(); ?>
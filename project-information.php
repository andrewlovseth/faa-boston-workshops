<?php

/*
	Template Name: Project Information
*/

get_header(); ?>

    <?php get_template_part('template-parts/project-information/anchor-links'); ?>

    <?php get_template_part('template-parts/project-information/project-background'); ?>

    <?php get_template_part('template-parts/project-information/final-documents'); ?>

    <?php get_template_part('template-parts/project-information/draft-documents'); ?>

    <?php get_template_part('template-parts/project-information/features'); ?>

    <?php get_template_part('template-parts/project-information/resources'); ?>

    <?php get_template_part('template-parts/global/video-overlay'); ?>

<?php get_footer(); ?>
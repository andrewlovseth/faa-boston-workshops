(function ($, window, document, undefined) {

	$(document).ready(function($) {

        $('.language-switcher a').on('click', function(){
            var tabTarget = $(this).attr('href');

            $('.language-switcher a').removeClass('active');
            $(this).addClass('active');

            $('.tab').removeClass('active');
            $(tabTarget).addClass('active');


            return false;
        });
			
	});

})(jQuery, window, document);
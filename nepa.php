<?php

/*
	Template Name: NEPA
*/

get_header(); ?>

	<?php get_template_part('template-parts/nepa/intro'); ?>
	
	<?php get_template_part('template-parts/nepa/study-info'); ?>

	<?php get_template_part('template-parts/nepa/study-diagram'); ?>

	<?php get_template_part('template-parts/nepa/assessment-info'); ?>

	<?php get_template_part('template-parts/nepa/assessment-diagram'); ?>	

<?php get_footer(); ?>